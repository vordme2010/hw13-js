const body = document.querySelector("body")
const changeThemeButton = document.querySelector(".theme-btn")
let currentTheme = window.localStorage.getItem('currentTheme') 
let currentButton = window.localStorage.getItem('currentButton') || "night theme &#127769"
body.classList.add(currentTheme)
changeThemeButton.innerHTML = currentButton
changeThemeButton.addEventListener("click", event => {
    body.classList.toggle("theme1")
    function addToStorageTheme(className) {
        window.localStorage.setItem(
            'currentTheme',
            currentTheme = className
        )
    }
    function addToStorageButton(text) {
        window.localStorage.setItem(
            'currentButton',
            currentButton = text
        )
    }
    if(body.classList.contains("theme1")) {
        window.addEventListener('beforeunload', addToStorageTheme("theme1"))
        event.target.innerHTML = "day theme &#9728"
        addToStorageButton("day theme &#9728")
    } else {
        window.addEventListener('beforeunload', addToStorageTheme("body"))
        event.target.innerHTML = "night theme &#127769"
        addToStorageButton("night theme &#127769")
    }
})